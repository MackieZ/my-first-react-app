import React, { Component } from "react";
import "./Note-add.css";
import PropTypes from "prop-types";
import "bulma/css/bulma.css";

class NoteAdd extends Component {
    constructor() {
        super();
        this.state = {
            newNoteContent: ""
        };
        
        this.handleUserInput = this.handleUserInput.bind(this);
        this.writeNote = this.writeNote.bind(this);
        console.log(this);
    }

    handleUserInput(e) {
        
        this.setState({
            newNoteContent: e.target.value
        });
    }

    writeNote() {
        this.props.addNote(this.state.newNoteContent);
        this.setState({
            newNoteContent: ""
        });
    }

    render() {
        
        return (
            
            <div className="container">
                <div className="columns is-two-thirds is-desktop">
                    <input
                        className="input is-info"
                        type="text"
                        placeholder="Add Notes"
                        value={this.state.newNoteContent}
                        onChange={this.handleUserInput}
                    />
                    <button
                        className="button is-primary"
                        onClick={this.writeNote}
                    >
                        Add
                    </button>
                </div>
            </div>
        );
    }
}

export default NoteAdd;
