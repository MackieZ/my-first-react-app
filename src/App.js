import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Note from "./Note/Note";
import NoteAdd from "./Note-add/Note-add";
import NoteEdit from './Note-edit/Note-edit';
import * as firebase from "firebase";

class App extends Component {
    constructor() {
        super();

        var config = {
            apiKey: "AIzaSyCRyfFHTNH81TtG5RiValPcrSbNKxxsGDY",
            authDomain: "my-first-react-9bbff.firebaseapp.com",
            databaseURL: "https://my-first-react-9bbff.firebaseio.com",
            projectId: "my-first-react-9bbff",
            storageBucket: "my-first-react-9bbff.appspot.com",
            messagingSenderId: "226834793444"
        };
        firebase.initializeApp(config);

        this.addNote = this.addNote.bind(this);
        this.removeNote = this.removeNote.bind(this);

        this.state = {
            notes: [
                { id: 1, noteContent: "Loading ..." },
                { id: 2, noteContent: "Loading ... " }
            ]
        };
    }

    componentDidMount() {
        this.rootRef = firebase
            .database()
            .ref()
            .child("speed");

        //READ DATA FROM FIREBASE
        this.rootRef.on("value", snap => {
            const array = [];
            snap.forEach(val => {
                array.push({
                    id: val.key,
                    noteContent: val.val().noteContent
                });
            });

            this.setState({
                notes: array
            });
        });
    }

    addNote(note) {
        this.rootRef.push().set({
            noteContent: note
        });
    }
    
    removeNote(noteId) {
        this.rootRef.child(noteId).remove();
    }

    render() {
        return (
            <div className="notesWrapper">
                <div className="notesHeader">Welcome to My Note !
                <NoteEdit />
                    </div>
                <div className="notesBody">
                    {this.state.notes.map(note => {
                        return (
                            <Note
                                noteContent={note.noteContent}
                                noteId={note.id}
                                key={note.id} 
                                removeNote={this.removeNote}
                            />
                        );
                    })}
                </div>
                <div className="notesFooter">
                    <NoteAdd addNote={this.addNote} />
                </div>
            </div>
        );
    }
}

export default App;
