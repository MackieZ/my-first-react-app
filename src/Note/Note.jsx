import React, { Component } from "react";
import "./Note.css";
import PropTypes from "prop-types";

class Note extends Component {
    constructor(props) {
        super(props);
        this.noteContent = props.noteContent;
        this.noteId = props.noteId;
        this.removeNote = this.removeNote.bind(this);
    }

    removeNote(id) {
        this.props.removeNote(id);
    }

    render(props) {
        return (
            <div className="note fade-in">
                <div className="noteContent">{this.noteContent}</div>
                <span
                    className="closebtn" 
                    onClick={() => this.removeNote(this.noteId)}
                >
                    &times;
                </span>
            </div>
        );
    }
}

this.PropTypes = {
    noteContent: PropTypes.string,
    noteId: PropTypes.any
};

export default Note;
